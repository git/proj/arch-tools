#!/usr/bin/env python3
# Copyright 2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

import datetime
import optparse
import os.path
import sys
import xmlrpc.client

import portage.versions

from common import login, detect_cpvs


def keyword_to_email(keyword):
	if keyword == 'arm64':
		keyword = 'arm'
	return '%s@gentoo.org' % keyword


if __name__ == "__main__":
	parser = optparse.OptionParser()
	parser.add_option("--days", dest="days", type=int, default=30, help="Number of days after maintainer timeout occurs.")
	(options, args) = parser.parse_args()
	if args:
		parser.error("unrecognized command-line args")

	url = 'https://bugs.gentoo.org/xmlrpc.cgi'
	print('You will be prompted for your Gentoo Bugzilla username and password (%s).' % url)
	bugzilla = xmlrpc.client.ServerProxy(url)
	user, login_data = login(bugzilla)
	
	params = {}
	params['Bugzilla_token'] = login_data['token']
	params['reporter'] = user
	params['summary'] = ['stabilize', 'stabilization', 'stable']
	params['resolution'] = ''
	bugs = bugzilla.Bug.search(params)['bugs']
	params = {}
	params['Bugzilla_token'] = login_data['token']
	params['ids'] = [bug['id'] for bug in bugs]
	comments = bugzilla.Bug.comments(params)
	for bug in bugs:
		# Skip bugs where stabilization seems to be already in progress.
		arch_found = False
		for arch in portage.archlist:
			if '%s@gentoo.org' % arch in bug['cc']:
				arch_found = True
				break
		if arch_found:
			continue

		# Skip bugs without STABLEREQ keyword.
		if 'STABLEREQ' not in bug['keywords']:
			continue

		# Skip bugs with comments, they may indicate objections or problem reports.
		if len(comments['bugs'][str(bug['id'])]['comments']) > 1:
			continue

		# Skip too recent bugs.
		creation_time = datetime.datetime.strptime(str(bug['creation_time']), '%Y%m%dT%H:%M:%S')
		if datetime.datetime.now() - creation_time < datetime.timedelta(days=options.days):
			continue

		cpvs = detect_cpvs(bug['summary'])
		if len(cpvs) != 1:
			continue

		target_keywords = set()
		cp = portage.versions.cpv_getkey(cpvs[0])
		for cpv in portage.portdb.cp_list(cp):
			for keyword in portage.portdb.aux_get(cpv, ['KEYWORDS'])[0].split():
				if '~' not in keyword and '-' not in keyword:
					target_keywords.add(keyword)

		params = {}
		params['Bugzilla_token'] = login_data['token']
		params['ids'] = [bug['id']]
		params['cc'] = {}
		params['cc']['add'] = list(set(keyword_to_email(k) for k in target_keywords))
		params['comment'] = {}
		params['comment']['body'] = 'Maintainer timeout (%d days). Arches please go ahead.' % options.days

		bugzilla.Bug.update(params)
		print('Updated bug #%s (%s). Target KEYWORDS: %s ;-)' % (
			bug['id'],
			bug['summary'],
			', '.join(list(target_keywords))))

	params = {}
	params['Bugzilla_token'] = login_data['token']
	bugzilla.User.logout(params)
