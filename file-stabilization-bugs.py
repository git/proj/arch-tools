#!/usr/bin/env python3
# Copyright 2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

import glob
import itertools
import optparse
import os
import pickle
import re
import shutil
import subprocess
import sys
import xmlrpc.client

from common import login
import portage.versions
from portage.xml.metadata import MetaDataXML

def save_state(done_cpvs):
	with open('file-stabilization-bugs.state', 'wb') as state_file:
		pickle.dump(done_cpvs, state_file)

if __name__ == "__main__":
	exit_code = 0

	parser = optparse.OptionParser()
	parser.add_option("-i", "--input", dest="input_filename", default="stabilization-candidates.txt", help="Input filename [default=%default]")
	parser.add_option("--repo", dest="repo", help="Path to portage CVS repository")

	(options, args) = parser.parse_args()
	if not options.input_filename:
		parser.error("--input option is required")
	if not options.repo:
		parser.error("--repo option is required")
	if args:
		parser.error("unrecognized command-line args")

	done_cpvs = []
	if os.path.exists('file-stabilization-bugs.state'):
		with open('file-stabilization-bugs.state', 'rb') as state_file:
			done_cpvs = pickle.load(state_file, fix_imports=True)

	url = 'https://bugs.gentoo.org/xmlrpc.cgi'
	print('You will be prompted for your Gentoo Bugzilla username and password (%s).' % url)
	bugzilla = xmlrpc.client.ServerProxy(url)
	user, login_data = login(bugzilla)
	
	with open(options.input_filename, "r") as input_file:
		for line in input_file:
			line = line.strip()

			# Skip empty/whitespace/comment lines.
			if not line or line.startswith("#"):
				continue

			cpv = line
			if cpv in done_cpvs:
				print('Skipping %s because it\'s marked as done' % cpv)
				continue

			cp = portage.versions.pkgsplit(cpv)[0]

			cvs_path = os.path.join(options.repo, cp)
			metadata = MetaDataXML(os.path.join(cvs_path, 'metadata.xml'), '/usr/portage/metadata/herds.xml')
			maintainer_split = metadata.format_maintainer_string().split(' ', 1)
			maintainer = maintainer_split[0]
			if len(maintainer_split) > 1:
				other_maintainers = maintainer_split[1].split(',')
			else:
				other_maintainers = []

			description = ('Is it OK to stabilize =%s ?\n' % cpv +
				       'If so, please CC all arches which have stable keywords\n' +
				       'for older versions of this package.')
			params = {}
			params['Bugzilla_token'] = login_data['token']
			params['product'] = 'Gentoo Linux'
			params['version'] = 'unspecified'
			params['component'] = 'Keywording and Stabilization'
			params['summary'] = '%s: stabilization request' % cpv
			params['description'] = description
			params['assigned_to'] = maintainer
			params['cc'] = other_maintainers
			params['severity'] = 'enhancement'
			try:
				bug_id = bugzilla.Bug.create(params)['id']
				print('Submitted bug #%d for %s. ;-)' % (bug_id, cpv))
				done_cpvs += cpv
				save_state(done_cpvs)
				try:
					params = {}
					params['Bugzilla_token'] = login_data['token']
					params['ids'] = [bug_id]
					params['keywords'] = {'set': 'STABLEREQ'}
					bugzilla.Bug.update(params)
				except xmlrpc.client.Fault as f:
					exit_code = 1
					print(f)
					print('Failed to add STABLEREQ keyword for %s. :-/' % cpv)
			except xmlrpc.client.Fault as f:
				exit_code = 1
				print(f)
				print('Failed to submit bug for %s. :-(' % cpv)
	
	if exit_code == 0 and os.path.exists('file-stabilization-bugs.state'):
		os.remove('file-stabilization-bugs.state')
	
	params = {}
	params['Bugzilla_token'] = login_data['token']
	bugzilla.User.logout(params)

	sys.exit(exit_code)
