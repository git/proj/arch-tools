#!/usr/bin/env python3
# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

import glob
import itertools
import optparse
import os
import pickle
import re
import shutil
import subprocess
import sys
import xmlrpc.client

from common import login
import portage.versions

BUG_REGEX = re.compile("[Bb]ug #?(\d+)")

def print_and_log(message, log):
	try:
		print(message)
		log.write(message + '\n')
	finally:
		log.flush()

def run_command(args, cwd, log):
	message = "Running %r in %s...\n" % (args, cwd)
	returncode = 0
	sys.stdout.write(message)
	log.write(message)

	try:
		output = subprocess.check_output(args, cwd=cwd,
				stderr=subprocess.STDOUT, universal_newlines=True)
	except subprocess.CalledProcessError as e:
		output = e.output
		returncode = e.returncode
	finally:
		log.write("Finished with exit code %d\n" % returncode)
		log.write(output)
		log.flush()

	return (returncode, output)
def save_state(done_bugs):
	with open('batch-stabilize.state', 'wb') as state_file:
		pickle.dump(done_bugs, state_file)

if __name__ == "__main__":
	parser = optparse.OptionParser()
	parser.add_option("--arch", dest="arch", help="Gentoo arch to use, e.g. x86, amd64, ...")
	parser.add_option("-i", "--input", dest="input_filename", default="package.keywords", help="Input filename for generated package.keywords file [default=%default]")
	parser.add_option("--repo", dest="repo", help="Path to portage CVS repository")
	parser.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False)

	(options, args) = parser.parse_args()
	if not options.arch:
		parser.error("--arch option is required")
	if not options.input_filename:
		parser.error("--input option is required")
	if not options.repo:
		parser.error("--repo option is required")
	if args:
		parser.error("unrecognized command-line args")

	done_bugs = []
	if os.path.exists('batch-stabilize.state'):
		with open('batch-stabilize.state', 'rb') as state_file:
			done_bugs = pickle.load(state_file, fix_imports=True)

	url = 'https://bugs.gentoo.org/xmlrpc.cgi'
	print('You will be prompted for your Gentoo Bugzilla username and password (%s).' % url)
	bugzilla = xmlrpc.client.ServerProxy(url)
	user, login_data = login(bugzilla)
	
	with open(options.input_filename, "r") as input_file:
		stabilization_dict = {}
		bug_id = -1
		for line in input_file:
			line = line.strip()

			# Skip empty/whitespace lines.
			if not line:
				continue

			if line.startswith("#"):
				match = BUG_REGEX.search(line, re.IGNORECASE)
				if not match:
					print('Ignoring comment line [%s]...' % line)
					continue
				else:
					bug_id = int(match.group(1))
				continue

			if bug_id == -1:
				print('Could not recognize bug id')
				sys.exit(1)

			# Drop the leading '='.
			cpv = line[1:]

			p = portage.versions.catsplit(cpv)[1]
			pn = portage.versions.pkgsplit(cpv)[0]
			ebuild_name = p + ".ebuild"
			if bug_id not in stabilization_dict:
				stabilization_dict[bug_id] = []
			stabilization_dict[bug_id].append((pn, ebuild_name))

		# Sanity check.
		success = True
		for bug_id in stabilization_dict:
			for (pn, ebuild_name) in stabilization_dict[bug_id]:
				ebuild_path = os.path.join(options.repo, pn, ebuild_name)
				if not os.path.exists(ebuild_path):
					print('%s: file does not exist' % ebuild_path)
					success = False
		if not success:
			print('Sanity check failed. Please make sure your CVS repo is up to date (cvs up).')
			sys.exit(1)

		with open('batch-stabilize.log', 'a') as log_file:
			for bug_id in stabilization_dict:
				if bug_id in done_bugs:
					print_and_log('Skipping bug #%d because it is marked as done.' % bug_id, log_file)
					continue

				print_and_log('Working on bug %d...' % bug_id, log_file)
				commit_message = "%s stable wrt bug #%d" % (options.arch, bug_id)
				for (pn, ebuild_name) in stabilization_dict[bug_id]:
					cvs_path = os.path.join(options.repo, pn)
					print_and_log('Working in %s...' % cvs_path, log_file)

					# Remove whole directory to prevent problems with conflicts.
					if os.path.exists(cvs_path):
						try:
							shutil.rmtree(cvs_path)
						except OSError:
							print('!!! rmtree %s failed' % cvs_path)
							sys.exit(1)

					if run_command(["cvs", "up", pn], options.repo, log_file)[0] != 0:
						print('!!! cvs up failed')
						sys.exit(1)
				for (pn, ebuild_name) in stabilization_dict[bug_id]:
					cvs_path = os.path.join(options.repo, pn)
					print_and_log('Working in %s...' % cvs_path, log_file)
					if run_command(["ekeyword", options.arch, ebuild_name], cvs_path, log_file)[0] != 0:
						print('!!! ekeyword failed')
						sys.exit(1)
					if run_command(["repoman", "manifest"], cvs_path, log_file)[0] != 0:
						print('!!! repoman manifest failed')
						sys.exit(1)
				for (pn, ebuild_name) in stabilization_dict[bug_id]:
					cvs_path = os.path.join(options.repo, pn)
					print_and_log('Working in %s...' % cvs_path, log_file)
					return_code, output = run_command(["cvs", "diff"], cvs_path, log_file)
					# It seems that cvs diff returns 1 if there are differences.
					if return_code == 0 and not output:
						print_and_log('Seems already keyworded, skipping.', log_file)
						done_bugs.append(bug_id)
						save_state(done_bugs)
						continue
					if run_command(["echangelog", commit_message], cvs_path, log_file)[0] != 0:
						print_and_log('echangelog failed, maybe just the Manifest is being updated; continuing', log_file)
					if run_command(["repoman", "manifest"], cvs_path, log_file)[0] != 0:
						print('!!! repoman manifest failed')
						sys.exit(1)
					if run_command(["repoman", "commit",
						"--include-arches", options.arch, "-m", commit_message], cvs_path, log_file)[0] != 0:
						print('!!! repoman commit failed')
						sys.exit(1)
				params = {}
				params['Bugzilla_token'] = login_data['token']
				params['ids'] = [bug_id]
				bug_xml = bugzilla.Bug.get(params)['bugs'][0]
				has_my_arch = False
				has_other_arches = False
				for cc in bug_xml['cc']:
					body, domain = cc.split('@', 1)
					if domain == 'gentoo.org':
						if body == options.arch:
							has_my_arch = True
						elif body in portage.archlist:
							has_other_arches=True

				if not has_my_arch:
					print_and_log('Seems that bugzilla has already been updated.', log_file)
					done_bugs.append(bug_id)
					save_state(done_bugs)
					continue

				print_and_log('Posting automated reply in bugzilla...', log_file)
				# We don't close bugs which still have other arches for obvious reasons,
				# and security bugs because stabilization is not the last step for them.
				params = {}
				params['Bugzilla_token'] = login_data['token']
				params['ids'] = [bug_id]
				params['cc'] = {}
				params['cc']['remove'] = ['%s@gentoo.org' % options.arch]
				params['comment'] = {}
				if has_other_arches or 'Security' in bug_xml['product']:
					params['comment']['body'] = '%s stable' % options.arch
					log_msg = 'Successfully updated'
				else:
					params['comment']['body'] = '%s stable, closing' % options.arch
					params['status'] = 'RESOLVED'
					params['resolution'] = 'FIXED'
					log_msg = 'Successfully updated and closed'

				bugzilla.Bug.update(params)
				print_and_log('%s bug %d.' % (log_msg, bug_id), log_file)
				done_bugs.append(bug_id)
				save_state(done_bugs)
	
	if os.path.exists('batch-stabilize.state'):
		os.remove('batch-stabilize.state')

		params = {}
		params['Bugzilla_token'] = login_data['token']
		bugzilla.User.logout(params)
